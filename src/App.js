import Chart from './Chart';
import PathFinder from './PathFinder';

import LAND_POLY from './data/land.json';

// FIRST:
// make the algorithm faster with these start and finish points.
import START from './data/start.json';
import FINISH from './data/finish.json';

import REEDS_BEACH from './data/reeds-beach.json';
import NEW_YORK from './data/new-york.json';
import SEA_ISLE_CITY from './data/sea-isle-city.json';

// THEN:
// See if you can make the algorithm fast enough for these start and finishes
// to also work...
// import START from './data/sea-isle-city.json';
// import FINISH from './data/reeds-beach.json';


// AND:
// How about starting from New York?
// import START from './data/new-york.json';



class App {

  constructor() {
    this.goButton = document.getElementById('go-button');
    this.goButton.addEventListener('click', this.go.bind(this));

    this.output = document.getElementById('output');

    this.pathFinder = new PathFinder();

    this.chart = new Chart();
    this.chart.addPoint('start', SEA_ISLE_CITY, 5, '#080');
    this.chart.addPoint('finish', REEDS_BEACH, 5, '#800');
    this.chart.addPoly('land', LAND_POLY, '#088');
  }


  go() {
    this.pathFinder.setStart(SEA_ISLE_CITY);
    this.pathFinder.setFinish(REEDS_BEACH);
    this.pathFinder.addExclusionZone(LAND_POLY);
    this.pathFinder.solve(this.chart, this.output);
  }

}

export default App;
