import cheapRuler from 'cheap-ruler';
import { boundingBoxAroundPolyCoords, intersectsWithPolygon, checkEqualPoints, EPS } from './geojson-utils';

import Heap from './Heap';
import TwoDimMap from './TwoDimMap';


class PathFinder {

  setStart(geoJSON) {
    this.ruler = cheapRuler(geoJSON.geometry.coordinates[1], 'meters');
    this.start = geoJSON.geometry;
  }

  setFinish(geoJSON) {
    this.finish = geoJSON.geometry;
  }


  addExclusionZone(geoJSON) {
    this.exclusion = geoJSON;
  }

  precalcBounds() {
    this.bounds = [];
    for (var i = 0; i < this.exclusion.features.length; ++i) {
      this.bounds.push(boundingBoxAroundPolyCoords(this.exclusion.features[i].geometry.coordinates[0]));
    }
  }

   buildPointsList() {
    this.points = [this.start.coordinates, this.finish.coordinates];
    for (var i = 0; i < this.exclusion.features.length; ++i) {
      for (var j = 0; j < this.exclusion.features[i].geometry.coordinates[0].length; ++j) {
        this.points.push(this.exclusion.features[i].geometry.coordinates[0][j]);
      }
    }
  }

  reset() {
    this.precalcBounds();
    this.buildPointsList();
    this.done = false;
    this.explored = new TwoDimMap();
    this.solution = [];
    this.distances = new Object();
    this.frontier = new Heap();
    this.distances[this.start.coordinates] = 0;
    this.frontier.insert(this.ruler.distance(this.start.coordinates, this.finish.coordinates), this.start.coordinates);
  }


  solve(chart, output) {
    output.innerText = 'STARTING PATH FINDER...' + this.start;
    this.startTime = Date.now();
    this.reset();

    while (!this.done) {
      this.iterate();
    }

    chart.addPoint('explored', GeoMultiPoint(this.explored.flatten()));
    chart.addLineString('route', GeoLineString(this.solution));
  }


  iterate() {
    this.current = this.frontier.extractMinimum();
    if (!this.current) {
      this.stop();
      return;
    }

    this.markAsExplored(this.current);
    // check if we should iterate this queue instance of the point
    if (this.checkDone(this.current) || 
      this.distances[this.current.value] + EPS < this.current.key - this.ruler.distance(this.current.value, this.finish.coordinates)) {
      return;
    }
    this.points.forEach(n => {
      if (this.isExplored(n)) {
        return;
      }
      var cost = this.distances[this.current.value] + this.ruler.distance(this.current.value, n);
      if (typeof this.distances[n] == null || this.distances[n] <= cost || this.pathIntersectsPoly(this.current.value, n)) {
        return;
      }

      this.distances[n] = cost;
      this.frontier.insert(cost + this.ruler.distance(n, this.finish.coordinates), n, this.current);
    });
  }

  stop() {
    output.innerText = 'PATH FOUND: ' + (Date.now() - this.startTime) + 'ms';
    this.done = true;
  }


  checkDone(node) {
    if (Date.now() - this.startTime > (60 * 1000)) {
      output.innerText = 'TIMED OUT'
      this.done = true;
    }
    if (checkEqualPoints(node.value, this.finish.coordinates)) {
      this.solution = [];
      this.buildSolution(node);
    }
    return this.done;
  }


  pathIntersectsPoly(startPoint, finishPoint) {
    /*console.log('START: ' + startPoint);
    console.log('FINISH: ' + finishPoint);*/
    const segment = [startPoint, finishPoint];
    for (var i = 0; i < this.exclusion.features.length; ++i) {
      if (intersectsWithPolygon(segment, this.exclusion.features[i].geometry.coordinates[0], this.bounds[i])) {
        return true
      }
    }
    return false
  }


  addToFrontier(cost, coordinates, parent) {
  }


  markAsExplored(node) {
    this.explored.add(node.value, node);
  }


  isExplored(coord) {
    return this.explored.get(coord) !== undefined;
  }


  buildSolution(node) {
    this.solution = [];
    let iter = node;
    while (iter) {
      this.solution.push(iter.value);
      iter = iter.parent;
    }
  }


}


function GeoPoint(coordinates) {
  return {
    'type': 'Feature',
    'properties': {},
    'geometry': {
      'type': 'Point',
      coordinates
    }
  }
}

function GeoLineString(coordinates) {
  return {
    'type': 'Feature',
    'properties': {},
    'geometry': {
      'type': 'LineString',
      coordinates
    }
  }
}


function GeoMultiPoint(coordinates) {
  return {
    'type': 'Feature',
    'properties': {},
    'geometry': {
      'type': 'MultiPoint',
      coordinates
    }
  }
}



export default PathFinder;
