
class Heap {

  // Creates a new binary heap with an optional customCompare.
  constructor(customCompare) {
    this.list = [];

    if (customCompare) {
      this.compare = customCompare;
    } else {
      this.compare = new KeyComparator();
    }
  }

  // Builds a heap with the provided key and value arrays, note that this
  // completely discards the old heap.
  buildHeap(keyArray, valueArray) {
    if (keyArray.length !== valueArray.length) {
      throw new Error('Key array must be the same length as value array');
    }

    var nodeArray = [];

    for (var i = 0; i < keyArray.length; i++) {
      nodeArray.push(new Node(keyArray[i], valueArray[i], i));
    }

    buildHeapFromNodeArray(this, nodeArray);
  }


  clear() {
    this.list.length = 0;
  }


  decreaseKey(node, newKey) {
    if (typeof node === 'undefined') {
      throw new Error('Cannot decrease key of non-existent node');
    }
    // Create a temp node for comparison
    if (this.compare.constructor.moreThan(newKey, node.key)) {
      throw new Error('New key is larger than old key');
    }

    node.key = newKey;
    var parent = getParent(node.i);
    while (this.compare.constructor.lessThan(node.key, this.list[parent].key)) {
      swap(this.list, node.i, parent);
      parent = getParent(node.i);
    }
  }


  delete(node) {
    // Bubble up to the root and extract
    while (node.i > 0) {
      var parent = getParent(node.i);
      swap(this.list, node.i, parent);
    }
    this.extractMinimum();
  }

  extractMinimum() {
    if (!this.list.length) {
      return undefined;
    }
    if (this.list.length === 1) {
      return this.list.shift();
    }
    var min = this.list[0];
    this.list[0] = this.list.pop();
    this.list[0].i = 0;
    heapify(this, 0);
    return min;
  }


  findMinimum() {
    return this.isEmpty() ? undefined : this.list[0];
  }


  insert(key, value, externalParent) {
    var i = this.list.length;
    var node = new Node(key, value, i, externalParent);
    this.list.push(node);
    var parent = getParent(i);
    while (parent !== i && this.compare.constructor.lessThan(
        this.list[i].key, this.list[parent].key)) {
      swap(this.list, i, parent);
      i = parent;
      parent = getParent(i);
    }
    //return node;
  }


  isEmpty() {
    return !this.list.length;
  }


  size() {
    return this.list.length;
  }

  union(otherHeap) {
    var array = this.list.concat(otherHeap.list);
    buildHeapFromNodeArray(this, array);
  }
}

// Use separate functions for <, > and === instead of a single compare()
// function as this minimises the number of comparisons needed.  This code is
// time-critical and must be as fast as possible
class KeyComparator {
  static lessThan(a, b) {
    return (a < b);
  }

  static moreThan(a, b) {
    return (a > b);
  }

  static equal(a, b) {
    return (a === b);
  }
}


function heapify(heap, i) {
  var l = getLeft(i);
  var r = getRight(i);
  var smallest = i;
  if (l < heap.list.length &&
      heap.compare.constructor.lessThan(heap.list[l].key, heap.list[i].key)) {
    smallest = l;
  }
  if (r < heap.list.length &&
      heap.compare.constructor.lessThan(
          heap.list[r].key, heap.list[smallest].key)) {
    smallest = r;
  }
  if (smallest !== i) {
    swap(heap.list, i, smallest);
    heapify(heap, smallest);
  }
}

// Builds a heap with the provided node array, note that this completely
// discards the old heap.
function buildHeapFromNodeArray(heap, nodeArray) {
  heap.list = nodeArray;
  for (var i = Math.floor(heap.list.length / 2); i >= 0; i--) {
    heapify(heap, i);
  }
}

function swap(array, a, b) {
  var temp = array[a];
  array[a] = array[b];
  array[b] = temp;
  array[a].i = a;
  array[b].i = b;
}

function getParent(i) {
  return Math.floor(i / 2);
}

function getLeft(i) {
  return 2 * i;
}

function getRight(i) {
  return 2 * i + 1;
}

function Node(key, value, i, parent) {
  this.key = key;
  this.value = value;
  this.i = i; // index needs to be tracked for decreaseKey and delete
  this.parent = parent;
}

export default Heap;