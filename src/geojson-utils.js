// compare complex floats expressions with eps
export const EPS = 0.000001

function signArea (a, b, c) {
  // for better accuracy scale coordinates
  const R = 1000;
  const x = [a[0] * R, a[1] * R];
  const y = [b[0] * R, b[1] * R];
  const z = [c[0] * R, c[1] * R];
  if (Math.abs((y[1] - x[1]) * (z[0] - x[0]) - (y[0] - x[0]) * (z[1] - x[1])) < EPS) {
    return 0
  }
  if ((y[1] - x[1]) * (z[0] - x[0]) - (y[0] - x[0]) * (z[1] - x[1]) > EPS) {
    return 1
  }
  if ((y[1] - x[1]) * (z[0] - x[0]) - (y[0] - x[0]) * (z[1] - x[1]) < -EPS) {
    return -1
  }
}

function compareBounnds (segmentFirst, segmentSecond) {
  // check segments bounds intersection first
  if (Math.max(segmentFirst[0][0], segmentFirst[1][0]) < Math.min(segmentSecond[0][0], segmentSecond[1][0])) {
    return false
  }
  if (Math.min(segmentFirst[0][0], segmentFirst[1][0]) > Math.max(segmentSecond[0][0], segmentSecond[1][0])) {
    return false
  }
  if (Math.max(segmentFirst[0][1], segmentFirst[1][1]) < Math.min(segmentSecond[0][1], segmentSecond[1][1])) {
    return false
  }
  if (Math.min(segmentFirst[0][1], segmentFirst[1][1]) > Math.max(segmentSecond[0][1], segmentSecond[1][1])) {
    return false
  }
  return true
}

function doIntersect (segmentFirst, segmentSecond) {
  if (!compareBounnds(segmentFirst, segmentSecond)) {
    return 0
  }

  // discard case when intersection is some polygon poin
  for (var i = 0; i < 2; ++i) {
    for (var j = 0; j < 2; ++j) {
      if (checkEqualPoints(segmentFirst[i], segmentSecond[j])) {
        return 1
      }
    }
  }

  // check sign areas
  if (signArea(segmentFirst[0], segmentFirst[1], segmentSecond[0]) * 
      signArea(segmentFirst[0], segmentFirst[1], segmentSecond[1]) > 0) {
    return 0
  }
  if (signArea(segmentSecond[0], segmentSecond[1], segmentFirst[0]) * 
      signArea(segmentSecond[0], segmentSecond[1], segmentFirst[1]) > 0) {
    return 0
  }
  return -1
}

// need to consider direction of a ray
function pnpoly (x, y, direction, coords) {
  var inside = false

  for (var i = 0; i < coords.length - 1; ++i) {
    // check with eps
    if (checkEqualFloats(coords[i][0], coords[i + 1][0]) || (coords[i][0] + EPS < y && coords[i + 1][0] + EPS < y) || (coords[i][0] > y + EPS && coords[i + 1][0] > y + EPS)) {
      continue
    }
    let xIntersect = (coords[i + 1][1] - coords[i][1]) * (y - coords[i][0]) / (coords[i + 1][0] - coords[i][0]) + coords[i][1];
    if (x < direction && x < xIntersect) {
      inside = !inside
    }
    if (x > direction && x > xIntersect) {
      inside = !inside
    }
  }

  return inside
}


export const intersectsWithPolygon = function (segment, coords, bounds) {
  // check bounding box intersection
  var intersectsWithBox = false;
  for (var i = 0; i < 4; ++i) {
    if (compareBounnds(segment, [bounds[i], bounds[i + 1]])) {
      intersectsWithBox = true;
    }
  }
  if (!intersectsWithBox) {
    return false;
  }
  // calc times when intersection is polygon point
  var vertexIntersect = 0
  for (i = 0; i < coords.length - 1; ++i) {
    let result = doIntersect(segment, [coords[i], coords[i + 1]])
    if (result == -1) {
      return true
    }
    vertexIntersect += result
  }
  // check if segment with end points as polygon points intersects with this polygon internally, it's enough to check for segment middle point
  if (vertexIntersect == 4 && pnpoly((segment[0][1] + segment[1][1]) / 2, (segment[0][0] + segment[1][0]) / 2, segment[1][1], coords)) {
    return true
  }

  return false
};

//some equality checks with scaling
export const checkEqualFloats = function (a, b) {
  const R = 1000000000;
  return Math.round(a*R) == Math.round(b*R)
}

export const checkEqualPoints = function (a, b) {
  const R = 1000000000;
  return Math.round(a[0]*R) == Math.round(b[0]*R) && Math.round(a[1]*R) == Math.round(b[1]*R);
}

// rewrote without this [[]], now returns bounding box as recatangular segments
export const boundingBoxAroundPolyCoords = function (coords) {
  var xAll = [], yAll = []
  for (var i = 0; i < coords.length; ++i) {
    xAll.push(coords[i][1])
    yAll.push(coords[i][0])
  }
  xAll = xAll.sort(function (a,b) { return a - b })
  yAll = yAll.sort(function (a,b) { return a - b })

  return [ [yAll[0], xAll[0]], [yAll[0], xAll[xAll.length - 1]], [yAll[yAll.length - 1], xAll[0]], [yAll[yAll.length - 1], xAll[xAll.length - 1]], [yAll[0], xAll[0]] ]
}

