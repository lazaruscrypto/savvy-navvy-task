# Getting set up

1. run
```
npm install
npm start
```
1. Now browse to http://127.0.0.1:4000/
1. Hit the 'FIND ROUTE' button in the top left
1. Wait for almost a minute (UX will freeze)
1. It should show dots the path finder explored, and a blue route to the finish
(if it times out - increase the timeout in PathFinder.js)


# Issues:

1. Even though deijkstra algorithm on grid with directions can give you good path planning approximation, this solution is only considering 8 directions, of course if there's no land and shortest past is simply line segment between two points this algorithm will return longer routing distance(depending on initial STEP, distance to finish and angle on grid between points if it's not multiple of 45 degrees).
2. value of STEP should be chosen carefully, as if STEP value too high the algorithm will go around finish point, if circle with center at this point inside some square with STEP sides, of course it depends on the routing algorithms but some approaches like RRT and simply finding the shortest path on the graph consisting of polygon points and start/finish points (the last one of the most complex, but will actually give the closest to exact shortest path)
3. value of DISTANCE_TO_FINISN, if it's to small and STEP is to high once again, it will be inside some square with STEP sides and the algorithm won't be able to reach it, of course it's desirable that value of STEP and DISTANCE_TO_FINISH are chosen based on obstacles (polygons) initially. 
4. There might be a case when distance between finish_point and current_point is less than DISTANCE_TO_FINISH, but the straight path between them intersects with some polygons and current solution doesn't check against it.
5. This solution builds bounding box each time when it checks if the point is on polygon (it takes O(nlogn) where is n is total number of polygons points), of course it can be done initially for all features and we will make simple check on coordinates for O(1)
6. Running the initial solution showed that algorithm built the path that even though it doesn't intersect with real land on the map, but it intersects with feature polygon, which is not correct. Algorithm checks if neighbour POINTS are inside polygons, actually it should check if segment from current point to neighbour point intersects with polygon, otherwise we can have examples where algorithm build the path through or too close to real land. It can be corrected just checking if segment intersects with any of polygon sides, it's also O(n) where n is total number of polygon points.
7. Do we even need this FIX_ROUNDING here if we still can have up to 0.1% error using spherical ruler.distance? Also we add and substarct 0.02, if we want ideal precision (whis is pointless) we can use big numbers.
8. This approach can waste lots of memory as we add any point (start_point_x + n * STEP, start_point_y + m * STEP), where n, m are integers. Also we can add a particular point many times to binary heap.
9. Algorithm of checking that point is inside polygon isn't correct, because it doesn't consider different ray directions when checking intersections, (intersection point can be before or after (x, y), but we're checking as if it's after x <).
10. Not an issue, but kinda misleading input format, because each polygon input has two instances of one point, it does makes iterating over polygon vertixes better and more efficient (don't need to calc (i + 1) % n or use if statement), but had to debug because of this moment for two-three hours until I checked the initial input.
11. This solution doesn't use EPS comparsion of float numbers for complex expression, which can result in incorrect results for functions like pnpoly, signArea, boundingBox checking and intersection checking and the result will be that we will lost optimal path or build incorrect path.
12. Small issue: sometimes borders of polygons don't correspond with land borders and we need to make borders for the whole continents)

Solutions:

1. Better data structures: we can sacrafice some memory for performance and add hashmap distances to our approauch. Hashmap distances will have point -> minimum_distance_from_start. Using this approach we don't need to upgrade points and add them to our binary heap if distances[Neighbour_Point] < distances[Current_Point] + distance(Current_Point, Neighbour_Point). When we extract new Current_Point from the heap, we can check if it's key >= distances[Current_Point] and skip in this case. If initial approach often adds the same points to binary heap (now it adds points to binary heap even minimal distance already less than upgradable), then our approach can even win in memory too. I'm using ES6 Object as most convinient way to emulate hashtable without writing it myself (though further to optimize performance one must implement his own hastable). FastMap from collections also probably will do fine.
2. Of course fix path intersection with polygons
3. Initial grid approach + optimizations like A*, Theta*, JSP, different values of STEP, DISTANCE_TO_FINISH
4. Build following graph: vertixes are points of polygons, start and finish points. Those points are sufficient to build shortest path, we "go around" polygon until we can reach another polygon without intersecting others or straight to finish point. This approach already considering any angle optimizations as we go to any direction with arbitary step. A* can be included into approach to check points in order of most potentially close to finish point. The goal is also to optimize function that checks if (current_point, next_point) segment intersects with any polygon. If the finish point outside of a particular polygon convex we can we can replace this polygon with its convex, it doesn't increase number of sides to check (will decrease it in most cases).
5. RRT also with A*, compared to previous approach we will check considerably more points increasing density.
6. JS improvements: using monomorphic functions and polymorphic functions where it's possible, fields of prototype should be initialized only in constructor (didn't optimize it for PathFinder, as we don't care about initial algorithm phase much), optimized Node constructor adding parent field there, better to use var declared variables for loops
7. I'm using 4th solution, though for each vertixes I consider and iterate over all vertixes in the graph, which of course isn't so efficient, but it can be compresse in the following way. It's clear that for each vertex of a polygon (if he is equal to itss convex), the neighbours are only it's neighbour vertices in the polygon. Also for each vertex (all polygon points, plus start/finish) we need to usually consider only two neighbours in each polygon, except the case when start/finish point is inside a non-convex polygon, as we just need to "go around" this polygon (the finish or start point is outside convex). To "go around" it optimally we need to choose between the leftest or rightest vertex the current vertex is seeing without obstacles and then move on sides of the convex until we can shift to next polygon or straight to finish point.
Considering the task input and time constraints it's not necessary to build compressed graph in this case, as we will have to write non-trivial (and so as in any geometry algorithm we can make mistakes hard to debug) methods to build neighbours for each vertex and it's still be at least O(n^2) or O(n * m * logn) (if we will build convexes around m polygons and use binary search to find the rightest and leftest points), where n is total number of vertexes, and our constant while making calculations will be huge and will have to play once again with precision.
8. Even better solution probably can be achieved via combining RRT or divide and conquer - divide the map into parts and make deikstra A* on geometrically compressed graph for those parts, then combine.

Solution overall wastes only up to ~10ms to find pathes for all cases even NEW YORK, and it's even without all possible optimizations. Will max optimizations I guess it's
possible to achieve up to ~3ms performance.