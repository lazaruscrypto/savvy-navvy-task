import resolve from 'rollup-plugin-node-resolve'
import json from 'rollup-plugin-json';
import commonjs from 'rollup-plugin-commonjs';
import babel from 'rollup-plugin-babel'

export default {
  input: './src/main.js',
  output: {
    file: './dist/bundle.js',
    format: 'cjs'
  },
  plugins: [
    resolve(),
    commonjs(),
    json({
      exclude: 'node_modules/**',
      preferConst: true,
      indent: '  ',
      compact: true,
      namedExports: true
    }),
    babel({
      // exclude: 'node_modules/**' // only transpile our source code
    })
  ]
}
